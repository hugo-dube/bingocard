import std.conv : to;
import vibe.core.log;
import vibe.data.json;
import vibe.http.router;
import vibe.http.server;
import vibe.http.fileserver;
import vibe.http.websockets;
import vibe.utils.array;

__gshared Card _card;

static this () {
    // Initialize the card
    _card = new Card();

    // Start the HTTP server
    auto settings = new HTTPServerSettings;
    settings.port = 4040;
    settings.bindAddresses = ["127.0.0.1"];

    auto router = new URLRouter;
    router.get("/", &index);
    router.get("/card", &card);

    router.post("/gen", &generate);
    router.get("/ws", handleWebSockets(&websocket));

    listenHTTP(settings, router);

    logInfo("Open http://localhost:4040/ in your web browser");
    logInfo("Open http://localhost:4040/card in a browser source/stream overlay");
}

void index (HTTPServerRequest req, HTTPServerResponse res) {
    res.renderDiet!"index.dt";
}

void card (HTTPServerRequest req, HTTPServerResponse res) {
    res.renderDiet!"card.dt"("image/svg+xml");
}

void generate (HTTPServerRequest req, HTTPServerResponse res) {
    import std.array : array;
    import std.digest.digest : digest;
    import std.digest.murmurhash : MurmurHash3;
    import std.random : Mt19937, partialShuffle, unpredictableSeed;
    import std.string : lineSplitter;

    struct Response {
        string message = "";
        bool success = true;

        this (string m) {
            this.message = m;
            this.success = false;
        }
    }

    debug {
        string seedParam = "test seed";
        string itemsParam = q"ITEMS
item 1
item 2
item 3
item 4
item 5
item 6
item 7
item 8
item 9
item 10
item 11
item 12
item 13
item 14
item 15
item 16
item 17
item 18
item 19
item 20
item 21
item 22
item 23
item 24
item 25
item 26
item 27
item 28
item 29
item 30
ITEMS";
    } else {
        string seedParam = req.form["seed"];
        string itemsParam = req.form["items"];
    }

    // If there's no provided seed text, use an unpredictable seed
    // Else take take the hash of the provided seed text
    uint seed = void;
    if (!seedParam || seedParam.length == 0) {
        seed = unpredictableSeed();
    } else {
        ubyte[4] tmp = digest!(MurmurHash3!32)(seedParam);
        seed = *(cast(uint*)(tmp.ptr));
    }

    // Split items text, one item by line
    string[] items = itemsParam.lineSplitter.array;
    if (items.length < 25) {
        res.writeJsonBody(Response("Need more than 25 items to generate a card."), 200);
        return;
    }

    // Shuffle 25 items from the list of items to pick the card's items
    Mt19937 rand;
    rand.seed(seed);
    items.partialShuffle(25, rand);

    // Push the new card data to all clients
    _card.setItems(items[0 .. 25]);
    _card.reset();
    _card.broadcastCard();

    res.writeJsonBody(Response());
}

void websocket (scope WebSocket socket) {
    _card.addClient(socket);
    scope (exit) {
        socket.close();
        _card.removeClient(socket);
    }

    while (socket.waitForData) {
        if (!socket.connected) { break; }

        auto text = socket.receiveText;

        _card.clientCmd(socket, text);
    }
}

class Card {
    string[5][5] _items;
    bool[5][5] _states;
    WebSocket[] _clients;

    struct Message {
        string command;
        Json data;
    }

    this () {
        reinit();
        reset();
    }

    void addClient (WebSocket socket) {
        _clients ~= socket;

        Message msg;
        msg.command = "card";
        msg.data = this.toJson();

        sendJson(socket, msg.serializeToJson());
    }

    void removeClient (WebSocket socket) {
        _clients.removeFromArray!WebSocket(socket);
    }

    void clientCmd (WebSocket socket, string cmdText) {
        try {
            auto message = parseJsonString(cmdText);

            string command = message["command"].get!string;
            switch (command) {
                case "itemstate":
                    int col = message["data"]["col"].get!int;
                    int row = message["data"]["row"].get!int;
                    bool state = message["data"]["state"].get!bool;

                    _states[col][row] = state;

                    broadcastText(cmdText);
                    break;
                default:
                    logInfo("Unknown client command: " ~ command);
                    break;
            }
        } catch (Exception e) {
            debug { logInfo(e.toString()); }
        }
    }

    private void sendText (WebSocket socket, string text) {
        socket.send(text);
    }

    private void sendJson (WebSocket socket, Json json) {
        sendText(socket, json.toString);
    }

    private void broadcastText (string text) {
        foreach (client; _clients) {
            sendText(client, text);
        }
    }

    private void broadcastJson (Json json) {
        broadcastText(json.toString);
    }

    void sendCard (WebSocket socket) {
        Message msg;
        msg.command = "card";
        msg.data = this.toJson();

        sendJson(socket, msg.serializeToJson());
    }

    void broadcastCard () {
        Message msg;
        msg.command = "card";
        msg.data = this.toJson();

        broadcastJson(msg.serializeToJson());
    }

    void reinit () {
        _items = [
            [ "", "", "", "", "" ],
            [ "", "", "", "", "" ],
            [ "", "", "", "", "" ],
            [ "", "", "", "", "" ],
            [ "", "", "", "", "" ]
        ];
    }

    void reset () {
        _states = [
            [false, false, false, false, false],
            [false, false, false, false, false],
            [false, false, false, false, false],
            [false, false, false, false, false],
            [false, false, false, false, false]
        ];
    }

    void setItems (string[] items) {
        assert(items.length == 25);

        foreach (row; 0 .. 5)
        foreach (col; 0 .. 5) {
            _items[col][row] = items[(row * 5) + col].idup;
        }
    }

    void setText (int col, int row, string text) {
        assert(col >= 0 && col < 5);
        assert(row >= 0 && row < 5);

        _items[col][row] = text;
    }

    void setState (int col, int row, bool state) {
        assert(col >= 0 && col < 5);
        assert(row >= 0 && row < 5);

        if (_states[col][row] != state) {
            _states[col][row] = state;
        }
    }

    void toggle (int col, int row) {
        assert(col >= 0 && col < 5);
        assert(row >= 0 && row < 5);

        _states[col][row] = !_states[col][row];
    }

    Json toJson () {
        struct ItemData {
            string text;
            bool done;
        }

        struct CardData {
            ItemData[5][5] card;
        }

        CardData data;

        foreach (row; 0 .. 5)
        foreach (col; 0 .. 5) {
            data.card[col][row] = ItemData(_items[col][row], _states[col][row]);
        }

        return data.serializeToJson();
    }
}

// Below this is code from vibe.d

void renderDiet (string template_file, ALIASES...) (HTTPServerResponse res, string contentType = "text/html; charset=UTF-8") {
    res.contentType = contentType;
    import vibe.stream.wrapper : streamOutputRange;
    import diet.html : compileHTMLDietFile;
    auto output = streamOutputRange!1024(res.bodyWriter);
    compileHTMLDietFile!(template_file, ALIASES, DefaultFilters)(output);
}

version (Have_diet_ng)
{
    import diet.traits;

    @dietTraits
    private struct DefaultFilters {
        import diet.html : HTMLOutputStyle;
        import std.string : splitLines;

        version (VibeOutputCompactHTML) enum HTMLOutputStyle htmlOutputStyle = HTMLOutputStyle.compact;
        else enum HTMLOutputStyle htmlOutputStyle = HTMLOutputStyle.pretty;

        static string filterCss(I)(I text, size_t indent = 0)
        {
            auto lines = splitLines(text);

            string indent_string = "\n";
            while (indent-- > 0) indent_string ~= '\t';

            string ret = indent_string~"<style type=\"text/css\">";
            indent_string = indent_string ~ '\t';
            foreach (ln; lines) ret ~= indent_string ~ ln;
            indent_string = indent_string[0 .. $-1];
            ret ~= indent_string ~ "</style>";

            return ret;
        }

        static string filterCssx(I)(I text, size_t indent = 0)
        {
            auto lines = splitLines(text);

            string indent_string = "\n";
            while (indent-- > 0) indent_string ~= '\t';

            string ret = indent_string~"<style type=\"text/css\"><![CDATA[";
            indent_string = indent_string ~ '\t';
            foreach (ln; lines) ret ~= indent_string ~ ln;
            indent_string = indent_string[0 .. $-1];
            ret ~= indent_string ~ "]]></style>";

            return ret;
        }


        static string filterJavascript(I)(I text, size_t indent = 0)
        {
            auto lines = splitLines(text);

            string indent_string = "\n";
            while (indent-- > 0) indent_string ~= '\t';

            string ret = indent_string~"<script type=\"application/javascript\">";
            ret ~= indent_string~'\t' ~ "//<![CDATA[";
            foreach (ln; lines) ret ~= indent_string ~ '\t' ~ ln;
            ret ~= indent_string ~ '\t' ~ "//]]>" ~ indent_string ~ "</script>";

            return ret;
        }

        static string filterMarkdown(I)(I text)
        {
            import vibe.textfilter.markdown : markdown = filterMarkdown;
            // TODO: indent
            return markdown(text);
        }

        static string filterHtmlescape(I)(I text)
        {
            import vibe.textfilter.html : htmlEscape;
            // TODO: indent
            return htmlEscape(text);
        }

        static this()
        {
            filters["css"] = (input, scope output) { output(filterCss(input)); };
            filters["cssx"] = (input, scope output) { output(filterCssx(input)); };
            filters["javascript"] = (input, scope output) { output(filterJavascript(input)); };
            filters["markdown"] = (input, scope output) { output(filterMarkdown(() @trusted { return cast(string)input; } ())); };
            filters["htmlescape"] = (input, scope output) { output(filterHtmlescape(input)); };
        }

        static SafeFilterCallback[string] filters;
    }


    unittest {
        static string compile(string diet)() {
            import std.array : appender;
            import std.string : strip;
            import diet.html : compileHTMLDietString;
            auto dst = appender!string;
            dst.compileHTMLDietString!(diet, DefaultFilters);
            return strip(cast(string)(dst.data));
        }

        assert(compile!":css .test" == "<style type=\"text/css\"><!--\n\t.test\n--></style>");
        assert(compile!":javascript test();" == "<script type=\"application/javascript\">\n\t//<![CDATA[\n\ttest();\n\t//]]>\n</script>");
        assert(compile!":markdown **test**" == "<p><strong>test</strong>\n</p>");
        assert(compile!":htmlescape <test>" == "&lt;test&gt;");
        assert(compile!":css !{\".test\"}" == "<style type=\"text/css\"><!--\n\t.test\n--></style>");
        assert(compile!":javascript !{\"test();\"}" == "<script type=\"application/javascript\">\n\t//<![CDATA[\n\ttest();\n\t//]]>\n</script>");
        assert(compile!":markdown !{\"**test**\"}" == "<p><strong>test</strong>\n</p>");
        assert(compile!":htmlescape !{\"<test>\"}" == "&lt;test&gt;");
        assert(compile!":javascript\n\ttest();" == "<script type=\"application/javascript\">\n\t//<![CDATA[\n\ttest();\n\t//]]>\n</script>");
    }
}
