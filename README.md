# Bingocard

Some bingo card thingamajig for speedrunners

---

### Instructions

1. Run the program
2. Open `localhost:4040` in your web browser of choice (tested on Firefox and Chrome)
3. Open `localhost:4040/card` in a browser source or other stream overlay in your streaming software of choice
4. Put a list of challenges in the "Items" text box, one per line
5. Optionally provide a seed for the random generator
6. Hit "Generate"
7. Click cells to mark their challenges as done
